#!/bin/sh

if [ -z "$JDK_HOME" ]
then
	echo '*** Warning!'
	echo '*** Set JDK_HOME (JDK >= 6) and PATH=$JDK_HOME/bin:$PATH if you want to build JDBC lib.'
else
	CXXFLAGS="-I$JDK_HOME/include -I$JDK_HOME/include/linux $CXXFLAGS"
	if ! echo $PATH | grep -qG '^'$JDK_HOME/bin; then
		echo '*** Warning!'
		echo '*** Please set PATH=$JDK_HOME/bin:$PATH if you want to use JDBC lib.'
		export PATH=$JDK_HOME/bin:$PATH
	fi
fi

set -e

./configure --prefix=`pwd`/install CXXFLAGS="-g -Wno-write-strings $CXXFLAGS"
make
make install
./csqlinstall.ksh
