#!/bin/sh
# Test Case
# 9. Create table t1 in mysql with primary key. Insert 10 rows. 
#    Cache the table in csql. 
#    Update 2 rows in csql and 2 rows in mysql with different primary keys. 
#    Run $ cacheverify -t t1. 
#        The output should display number of records as 10 for both csql and 
#        mysql respectively.
#    Run $ cacheverify -t t1 -p. 
#        The output should display no missing records in either of the 
#        databases.
#    Run $ cacheverify -t t1 -f. 
#        The output should display the 4 inconsistent records. 
#        Only those fields with different values should be displayed 
#        for inconsistent records.   

#Run this test only under csql/test or on this directory.
#Otherwise, it may fail
 
input=${PWD}/cache/CacheVerify/mysqlinput.sql
REL_PATH=.
if [ -s "$input" ]
then
    REL_PATH=${PWD}/cache/CacheVerify
fi

cp $CSQL_CONFIG_FILE /tmp/csql.conf
echo DSN=$DSN >>$CSQL_CONFIG_FILE
isql $DSN < ${REL_PATH}/mysqlinput.sql >/dev/null 2>&1
if [ $? -ne 0 ]
then
   cp /tmp/csql.conf $CSQL_CONFIG_FILE
    exit 1;
fi

$CSQL_INSTALL_ROOT/bin/csql -s $REL_PATH/mysqlinput.sql >/dev/null 2>&1
if [ $? -ne 0 ]
then
   cp /tmp/csql.conf $CSQL_CONFIG_FILE
    exit 2;
fi

echo "1:t1 NULL NULL NULL" > /tmp/csql/csqltable.conf
isql $DSN < ${REL_PATH}/mysqlupdate.sql >/dev/null 2>&1
if [ $? -ne 0 ]
then
   cp /tmp/csql.conf $CSQL_CONFIG_FILE
    exit 3;
fi


$CSQL_INSTALL_ROOT/bin/csql -s $REL_PATH/csqlupdate.sql >/dev/null 2>&1
if [ $? -ne 0 ]
then
   cp /tmp/csql.conf $CSQL_CONFIG_FILE
    exit 4;
fi

$CSQL_INSTALL_ROOT/bin/cacheverify -t t1
if [ $? -ne 0 ]
then
   cp /tmp/csql.conf $CSQL_CONFIG_FILE
    exit 5;
fi

$CSQL_INSTALL_ROOT/bin/cacheverify -t t1 -p
if [ $? -ne 0 ]
then
   cp /tmp/csql.conf $CSQL_CONFIG_FILE
    exit 6;
fi

$CSQL_INSTALL_ROOT/bin/cacheverify -t t1 -f
if [ $? -ne 0 ]
then
   cp /tmp/csql.conf $CSQL_CONFIG_FILE
    exit 7;
fi

rm -f /tmp/csql/csqltable.conf
touch /tmp/csql/csqltable.conf
isql $DSN < $REL_PATH/drop.sql >/dev/null 2>&1
$CSQL_INSTALL_ROOT/bin/csql -s $REL_PATH/drop.sql >/dev/null 2>&1

   cp /tmp/csql.conf $CSQL_CONFIG_FILE
exit 0;
