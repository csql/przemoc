#!/bin/sh
# Test Case

# 9.create table t1 (f1 int, f2 int) in mysql and 
#   create table t1 (f1 int, f2 int, f3 int) in csql. 
#   load with no definition option. should fail.

#Run this test only under csql/test or on this directory.
#Otherwise, it may fail
dropAll() {
rm -f /tmp/csql/csqltable.conf /tmp/csql/csql.db
touch /tmp/csql/csqltable.conf /tmp/csql/csql.db
#cp $CSQL_CONFIG_FILE /tmp/csql.conf
#echo DSN=$DSN >>$CSQL_CONFIG_FILE
isql $DSN < ${REL_PATH}/dropall.sql >/dev/null 2>&1
$CSQL_INSTALL_ROOT/bin/csql -s $REL_PATH/dropall.sql > /dev/null 2>&1
#cp /tmp/csql.conf $CSQL_CONFIG_FILE
}


input=${PWD}/cache/CacheTable/create.sql
REL_PATH=.
if [ -s "$input" ]
then
    REL_PATH=${PWD}/cache/CacheTable
fi

cp $CSQL_CONFIG_FILE /tmp/csql.conf
echo DSN=$DSN >>$CSQL_CONFIG_FILE
echo CACHE_TABLE=true >>$CSQL_CONFIG_FILE
isql $DSN < ${REL_PATH}/create.sql >/dev/null 2>&1
echo table t1 and t2 are created with records in target db

rm -f /tmp/csql/csqltable.conf /tmp/csql/csql.db
touch /tmp/csql/csqltable.conf /tmp/csql/csql.db

# write to csqltable.conf
echo "1:t1 NULL NULL NULL" > /tmp/csql/csqltable.conf
echo "2:t2 NULL t2f1<5 NULL" >> /tmp/csql/csqltable.conf

$CSQL_INSTALL_ROOT/bin/cachetable -R 
if [ $? -ne 0 ]
then
    dropAll
cp /tmp/csql.conf $CSQL_CONFIG_FILE
    exit 1;
fi

$CSQL_INSTALL_ROOT/bin/cachetable -t t1 -u 
if [ $? -ne 0 ]
then
    dropAll
cp /tmp/csql.conf $CSQL_CONFIG_FILE
    exit 2;
fi
echo table t1 unloaded
$CSQL_INSTALL_ROOT/bin/cachetable -t t2 -u 
if [ $? -ne 0 ]
then
    dropAll
cp /tmp/csql.conf $CSQL_CONFIG_FILE
    exit 3;
fi
echo table t2 unloaded
grep t1 /tmp/csql/csqltable.conf
if [ $? -eq 0 ]
then 
    dropAll
cp /tmp/csql.conf $CSQL_CONFIG_FILE
    exit 4;
fi

grep t2 /tmp/csql/csqltable.conf
if [ $? -eq 0 ]
then 
    dropAll
cp /tmp/csql.conf $CSQL_CONFIG_FILE
    exit 5;
fi
dropAll
cp /tmp/csql.conf $CSQL_CONFIG_FILE
exit 0;

